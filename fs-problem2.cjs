const fs = require("fs");
const path = require("path");

/*  Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the 
        new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into
         sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name 
        of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are
         mentioned in that list simultaneously.   */

function readWriteModifyFiles(fileName)
{
  readFile(fileName)
  .then((fileData)=>{

   // console.log(filesName);

    return contentConvert(fileData)
  })
  .then((fileName1)=>{
   // console.log(fileName)
      return contentConvertAgain(fileName1)

   })
   .then((fileName2)=>{
 
        return contenetConvert2(fileName2)
   }).
   then((file)=>{
        deleteFiles(file)
   })
   .catch((error)=>{
      console.log(error);
   })
  }

function readFile(filename) {
  return new Promise((resolve , reject)=>{

      
    fs.readFile(`${filename}`, "UTF-8", (error, data) => {
      if (error) 
      {
        reject(error);
      } else 
      {
        console.log(data);

        resolve(data);
      }
    });
  })
}

function contentConvert(fileData){
  
  return new Promise((resolve , reject)=>{

    fs.writeFile("filenames.txt", "", (error) => {
      if (error) 
      {
        reject(error);
      }
    });

      let newFileName = "newFile1.txt";
        fs.writeFile(`${newFileName}`, `${fileData}`.toUpperCase(), (error) => {
           if (error) 
          {
             reject(error);
          }

              fs.appendFile("filenames.txt", `${newFileName} `, (error) => {
              if (error) 
              {
                  reject(error);
              }else
              {
                resolve(newFileName);
              }
         });

      });

    });
}  

function contentConvertAgain(fileName)
{
    return new Promise((resolve, reject)=>{
        
      fs.readFile(`${fileName}`, "UTF-8", (error, data) => {
                if (error) 
                {
                    reject(error)
                } else
                 {
                  console.log('\n')
                  console.log(data);

                   let convert = data.toLowerCase().split(". ");
                   let convertString = convert.join("\n");

                   const fileName2 = "newFile2.txt";
                    fs.writeFile(`${fileName2}`, `${convertString}`, (error) => {
                       if (error) 
                       {
                          reject(error)
                       }

                       fs.appendFile("filenames.txt", `${fileName2} `, (error) => {
                            if (error)
                             {
                              reject(error)
                             }
                             else
                             {
                                  resolve(fileName2);
                             }
                        });

                       

                    });
                  }
            }); 
       })       
}

function contenetConvert2(fileName2)
{
     return new Promise((resolve, reject)=>{

        fs.readFile(`${fileName2}`, "UTF-8", (error, data) => {
            if (error) 
            {
                reject(error);
            } else 
            {
                console.log('\n');
                console.log(data);

                let content = data;
                let sortedContent = content.split('.').sort();

                let sortedcontentString = sortedContent.join("");
                //  console.log(sortedcontent)

                let fileName3 = "newFile3.txt";
                fs.writeFile(`${fileName3}`, `${sortedcontentString}`, (error) => {
                  if (error) 
                   {
                       reject(error);
                   }
                   
                   fs.appendFile("filenames.txt", `${fileName3}`, (error) => {
                      if (error) 
                      {
                          reject(error);
                      }else
                      {
                        resolve('filenames.txt');
                      }
                   });
                }); 
            }   
        });
     });  
}

function deleteFiles(file)
{
    return new Promise((resolve, reject)=>{

      fs.readFile("filenames.txt", "UTF-8", (error, data) => {
         if (error) 
          {
             reject(error);
          } else 
          {
            console.log('\n');
              console.log(data);

              let fileInTxt = data.split(" ");
              for(let index = 0; index < fileInTxt.length; index++)
              {
                if (fileInTxt[index] !== "")
                 {
                    fs.unlink(`${fileInTxt[index]}`, (error) => {
                        if (error) 
                        {
                          reject(error);
                        } 
                    })
                 }    
              }

              fs.unlink("filenames.txt", (error) => {
                 if (error) 
                 {
                    reject(error);
                 }
                 else
                 {
                    console.log('\n');
                     console.log('All files deleted successfully')
                     resolve('files deleted successfully')
                 }
              });
           }
       })
    })   
}

module.exports = readWriteModifyFiles;

