
const fs = require("fs");
const path = require("path");

function createAndDeleteFiles(directory){
   createDirectory(directory)
   .then((numberOfFiles)=>{

      console.log('directory created ');

    return createDifferentFiles(directory, numberOfFiles) 

   })
   .then((storedFileName)=>{

       console.log('All files created successfully');

    return deleteFiles(storedFileName, directory)

   })
   .then((data)=>{
       console.log(data);

       deleteDirectory(directory)
   })
   .catch((err)=>{
    console.log(err);
   })
}

//   function for creating new Directory
const createDirectory =(directory)=> {
   return new Promise((resolve, reject) =>{
      
    fs.mkdir(directory, (error) => {
      if (error) 
      {
        reject(err);
      } else
       {
        const numberOfFiles= Math.floor(Math.random()*10 +1);
        console.log(numberOfFiles);

        resolve(numberOfFiles);
      }
    })    
   })
}

//    function for creating files 
function createDifferentFiles(directory, numberOfFiles) {

  return new Promise((resolve, reject)=>{
 
    let executeCount = 0;
    let errorCount = 0;
    let storedFileName = [];
    //    for creating multiple files
    for (let index = 0; index < numberOfFiles; index++) 
    {
        const filename = `${index + 1}.json`;
    
        fs.writeFile(`${directory}/${filename}`, "", (error, data) => {
           if (error) 
           {
              errorCount += 1;
              reject('The error is ', err)
           } else 
           {
               executeCount += 1;
               storedFileName.push(filename);
           }

      // this is the condition when callback function called , after all files created
           if (executeCount + errorCount == numberOfFiles)
           {
              resolve(storedFileName);
           }
       });
    }
  })
}

 //    function for deleting files 
function deleteFiles(storedFileName, directory) {

  return new Promise((resolve, reject)=>{

     let count =0;
    for (let index = 0; index < storedFileName.length; index++) {
      fs.unlink(`${directory}/${storedFileName[index]}`, (error) => {
        if(error)
        {
           reject(error);
        }else
        {
           count++;
        }
        
  
        if (count == storedFileName.length) 
        {
          resolve('All files deleted successfully !')
        }
      });
    }
  })
}
  
//    function for deleting Directory
function deleteDirectory(directory) {
  return new Promise((resolve, reject)=>{
    fs.rmdir(`${directory}`, (error) => {
      if (error) 
      {
        reject(error);

      } else 
      {
        console.log("Directory deleted successfully !")
          resolve(' Directory deleted');
      }
    });
  })
}
module.exports = createAndDeleteFiles;


